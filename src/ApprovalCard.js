import React, {Component} from 'react';

export default class ApprovalCard extends React.Component{
    render(){
        let { children } = this.props;
        console.log('==>', this.props);
        
        return(
            <div className="ui card">
                <div className="content">
                    {children}
                </div>
                <div className="extra content"> 
                    <div className="ui two buttons">
                        <div className="ui basic green button">Approve</div>
                        <div className="ui basic red button">Decline</div>
                    </div>
                </div>
            </div>
        )
    }
}