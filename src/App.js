import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';
import faker from 'faker';
import CommentDetail from './CommentDetail';
import ApprovalCard from './ApprovalCard';

class App extends React.Component{
  render(){
    return(
      <div>
        <ApprovalCard>
          <div>
            <h4>Warning!</h4>
            <div>Are you sure?</div>
          </div>
        </ApprovalCard>

        <ApprovalCard>
          <CommentDetail author="Andre" avatar={faker.image.avatar()}/>
        </ApprovalCard>

        <ApprovalCard>
          <CommentDetail author="Setiawan" avatar={faker.image.avatar()}/>
        </ApprovalCard>

        <ApprovalCard>
          <CommentDetail author="Keren" avatar={faker.image.avatar()}/>
        </ApprovalCard>
      </div>
    )
  }
}

export default App;