import React, {Component} from 'react';
import faker from 'faker';

export default class CommentDetail extends React.Component{
    render(){
        let {author, avatar} = this.props;
        console.log(this.props);
        return(
            <div className="ui container comments" >
                <div className="comment">
                    <a href="/" className="avatar">
                        <img alt="avatar" src={avatar} />
                    </a>
                    <div className="content">
                        <a href="/" className="author">
                            {author}
                        </a>
                        <div className="metadata">
                            <span className="date">1:20PM</span>
                        </div>
                    </div>
                </div>
            </div >
        )
    }
}